<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Registro';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'username')->input('email') ?>

<?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

<?php
if ($model->isAdmin = null) {
    $model->isAdmin = 0;
}
?>

<div class="form-group">
    <?= Html::submitButton('Registrarse', ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>