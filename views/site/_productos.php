<?php

use yii\helpers\Html;

?>
<div class="productos">

    <h3><?= $dato['nombre'] ?></h3>

    <?php
    if ($dato['oferta']) {

    ?>
        <h3>Precio: &nbsp;<span style="color: red;"><del><?= $dato['precioeuros'] ?></del></span> <span style="margin-left: 10px;"><?= $dato['precioRebajado'] ?> €</span></h3>
    <?php
    } else {
    ?>
        <h3>Precio: &nbsp;<span><?= $dato['precioeuros'] ?></span> </h3>
    <?php
    }

    ?>
    <p>
        <?php
        if (isset($dato['foto'])) {
            echo Html::img("@web/imgs/productos/{$dato['foto']}", ['style' => 'width:200px;height: 200px', 'class' => 'img-thumbnail']);
        } else {
            echo  Html::img("@web/imgs/notfound.jpg", ['style' => 'width:200px;height: 200px', 'class' => 'img-thumbnail']);
        }
        ?>
    </p>

    <?= Html::a('Más detalles', ['site/verproducto', 'idProducto' => $dato->idProducto], ['class' => 'botonDetalles']) ?>

</div>