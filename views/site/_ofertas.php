<?php

use yii\helpers\Html;
?>
<tr>
    <td class="nombre"> <?= $model->nombre ?></td>
    <td> <del><?= $model->precioEuros ?></del></td>
    <td> <?= $model->precioRebajado . ' €' ?></td>
    <td class="imagen"> <?= Html::img("@web/imgs/productos/" . $model->foto, ['style' => 'width:200px;height:200px']) ?></td>
    <td>
        <p>
            <?= Html::a('Ver detalles del producto', ['site/verproducto', 'idProducto' => $model->idProducto], ['class' => 'btn btn-success']) ?>
        </p>
        <p>
            <?= Html::a('Comprar <i class="fas fa-cart-plus"></i>', ['site/hacerpedido', 'idProducto' => $model->idProducto, 'idUsuario' => Yii::$app->user->id], ['class' => 'btn btn-info text-dark', 'style' => 'width:']) ?>
        </p>
    </td>
</tr>