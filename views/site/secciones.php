<?php

use yii\helpers\Html;

?>
<div class="row">
    <?php
    foreach ($secciones as $dato) {
    ?>
        <div class="col-6 mb-3">
            <div class="card cartas">
                <div class="card-body">

                    <h5 class="card-title"><?= $dato->nombre ?></h5>
                    <?= Html::a('Ver productos', ['site/productos', 'id' => $dato->idSeccion], ['class' => 'btn btn-primary']) ?>

                </div>
            </div>
        </div>
    <?php
    }
    ?>
</div>