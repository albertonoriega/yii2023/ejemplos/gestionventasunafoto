<?php

use yii\helpers\Html;

foreach ($secciones as $seccion) {
?>
    <h2 class="oferta">Productos <?= $seccion->nombre ?> en oferta</h2>
    <table class="tablaOfertas">

        <tr>
            <td>Nombre</td>
            <td>Precio</td>
            <td>Precio rebajado</td>
            <td>Foto</td>
            <td></td>
        </tr>
        <?php
        foreach ($seccion->productosOferta as $producto) {
        ?>

            <tr>
                <td class="nombre"> <?= $producto->nombre ?></td>
                <td> <del><?= $producto->precioEuros ?></del></td>
                <td> <?= $producto->precioRebajado . ' €' ?></td>
                <td class="imagen"> <?= Html::img("@web/imgs/productos/" . $producto->foto, ['style' => 'width:200px;height:200px']) ?></td>
                <td> <?= Html::a('Ver detalles del producto', ['site/verproducto', 'idProducto' => $producto->idProducto], ['class' => 'botonDetalles']) ?></td>
            </tr>
        <?php
        }
        ?>
    </table>

<?php


}
