<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Pedido $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="pedido-form">

    <?php $form = ActiveForm::begin(); ?>



    <?= $form->field($model, 'idProducto')->dropDownList(
        [$producto->idProducto => $producto->nombre],
        ['readonly' => true]
    ) ?>

    <?= $form->field($model, 'idCliente')->dropDownList(
        [$cliente->idCliente => $cliente->nombre],
        ['readonly' => true]
    ) ?>

    <?= $form->field($model, 'cantidad')->input('number') ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>