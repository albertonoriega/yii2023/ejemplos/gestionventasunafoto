<?php

use yii\helpers\Html;
?>

<h2 style="text-align: center;margin:25px"> <?= $dato->nombre ?></h2>
<div class="fotoVerProducto" style="float:right;"><?= Html::img("@web/imgs/productos/{$dato->foto}", ['style' => 'width:320px;height:320px', 'class' => 'img-fluid']) ?></div>

<div class="contenedorTablaProducto">
    <table class="tablaProducto">
        <tbody>
            <tr>
                <td>Descripción</td>
                <td><?= $dato->descripcion ?></td>
            </tr>
            <tr>
                <td>Precio</td>
                <td><?= $dato->precio ?> €</td>
            </tr>
            <tr>
                <td>Descuento</td>
                <td><?php

                    echo ($dato->descuento != 0 && $dato->descuento != null) ?  $dato->descuento . ' %' : '';
                    ?>
                </td>
            </tr>
            <tr>
                <td>Precio rebajado</td>
                <td><?= $dato->precioRebajado ?> €</td>
            </tr>
            <tr>
                <td>Unidades en Stock</td>
                <td> <?= $dato->unidadesStock ?></td>
            </tr>
            <tr>
                <td>Sección</td>
                <td> <?= $dato->seccion0->nombre ?></td>
            </tr>
            <tr>
                <td colspan="2">
                    <?= Html::a('Comprar <i class="fas fa-cart-plus"></i>', ['site/hacerpedido', 'idProducto' => $dato->idProducto, 'idUsuario' => Yii::$app->user->id], ['class' => 'btn btn-info text-dark', 'style' => 'width:']) ?>
                </td>
            </tr>
        </tbody>

    </table>
</div>