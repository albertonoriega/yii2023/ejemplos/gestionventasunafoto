<div class="contenedor">

    <h2> <?= isset($titulo) ? $titulo : "Productos" ?></h2>
    <div class="tablaProductos">
        <?php

        foreach ($datos as $dato) {
        ?>

        <?php
            echo $this->render('_productos', [
                'dato' => $dato,
            ]);
        }

        ?>
    </div>
</div>