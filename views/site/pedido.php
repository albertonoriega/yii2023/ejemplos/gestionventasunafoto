<?php

use yii\helpers\Html;

$this->title = 'Pedido realizado correctamente';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<?php
(!empty($nombre)) ? '<p> Hola' . Html::encode($nombre) . '</p>' : '';
?>

<p>Tu pedido se ha realizado correctamente</p>

<?= Html::a('Ver mis pedidos', ['site/verpedidos', 'idCliente' => $idCliente], ['class' => 'btn btn-primary']) ?>