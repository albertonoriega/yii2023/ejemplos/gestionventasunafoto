<div class="contenedor">

    <?php

    use app\models\Producto;
    use yii\bootstrap5\Carousel;
    use yii\helpers\Html;

    include '_menuSecciones.php';

    ?>


    <div class="carousel">
        <?php

        //$datos = Producto::find()->where(['destacado' => 1, 'seccion' => $seccion])->all();


        // carousel
        $items = [];
        foreach ($productos as $producto) {
            $items[] = [
                "content" => Html::img("@web/imgs/productos/" . $producto->foto, ['class' => 'd-block m-auto', 'style' => 'width:250px;height:300px']),
                "caption" => mostrarPrecios($producto),
                "options" => []
            ];
        }
        echo Carousel::widget([
            "items" => $items,
            "options" => [
                "class" => 'carousel-dark col-3 '
            ],
        ]);
        ?>

    </div>

</div>
<?php
function mostrarPrecios($producto)
{
    if ($producto->oferta) {
        return Html::a("<div class='bg-secondary text-white fit'> <span>{$producto->nombre}</span> <span style='color:black;font-size:0.8em'><del>{$producto->precioEuros}</del></span> <span>{$producto->precioRebajado} €</span> </div>", ['site/verproducto', 'idProducto' => $producto->idProducto],);
    } else {
        return  Html::a("<div class='bg-secondary text-white fit'> <span>{$producto->nombre}</span> <span style='color:black;'>{$producto->precioEuros}</span> </div>", ['site/verproducto', 'idProducto' => $producto->idProducto],);
    }
}
