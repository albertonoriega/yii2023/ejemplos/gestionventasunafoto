<?php

use yii\grid\GridView;
use yii\helpers\Html;

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'idProducto',
            'value' => function ($model) {
                return $model->idProducto0->productoconseccion;
            }
        ],
        'cantidad',
        [
            'attribute' => 'Precio/unidad',
            'value' => function ($model) {
                return $model->idProducto0->precioEuros;
            }
        ],
        [
            'attribute' => 'Precio total',
            'value' => function ($model) {
                return $model->idProducto0->precio * $model->cantidad . ' €';
            }
        ],
        [
            'attribute' => 'fechaPedido',
            'value' => function ($model) {
                return $model->fechaEspana($model->fechaPedido);
            }
        ],
        //'fechaEnvio',
        [
            'attribute' => 'fechaEnvio',
            'value' => function ($model) {
                return $model->fechaEspana($model->fechaEnvio);
            }
        ],
        [
            'attribute' => '',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a('Eliminar pedido', ['cancelarpedido', 'idPedido' => $model->idPedido], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => '¿Estás seguro que quieres eliminar el pedido?',
                        'method' => 'post',
                    ],
                ]);
            }
        ]

    ],
]);
