<?php

use yii\helpers\Html;

$this->title = 'Mi cuenta';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cuenta">

    <h1><?= Html::encode($this->title) ?></h1>

    <div>
        <span> Nombre:</span> <?= $model->nombre ?>
    </div>
    <div>
        <span> Apellidos:</span> <?= $model->apellidos ?>
    </div>
    <div>
        <span>Correo:</span> <?= $model->correo ?>
    </div>
    <div>
        <span> Dirección:</span> <?= $model->direccion ?>
    </div>
    <div>
        <span> Población:</span> <?= $model->poblacion ?>
    </div>
    <div>
        <span> Código postal:</span> <?= $model->codigoPostal ?>
    </div>
    <div>
        <span> Teléfono móvil:</span> <?= $model->telefono ?>
    </div>
    <div>
        <span> Teléfono:</span> <?= $model->telefono2 ?>
    </div>

    <div>
        <?= Html::a('Actualizar datos', ['site/actualizarcuenta', 'idCliente' => $model->idCliente], ['class' => 'btn btn-success']) ?>
    </div>



</div>