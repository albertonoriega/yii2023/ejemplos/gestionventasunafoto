<?php

use Codeception\Attribute\DataProvider;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\ListView;

foreach ($secciones as $seccion) {
?>
    <h2 class="oferta">Productos <?= $seccion->nombre ?> en oferta</h2>
    <table class="tablaOfertas">

        <tr>
            <td>Nombre</td>
            <td>Precio</td>
            <td>Precio rebajado</td>
            <td>Foto</td>
            <td></td>
        </tr>
        <?php

        echo ListView::widget([
            "dataProvider" => new ActiveDataProvider([
                "query" => $seccion->getProductosOferta(),
                'pagination' => [
                    'pageSize' => 1,
                    'pageParam' => $seccion->nombre
                ],
            ]),
            "itemView" => "_ofertas"
        ]);

        ?>
    </table>

<?php


}
