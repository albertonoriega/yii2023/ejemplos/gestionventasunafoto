<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Pedido $model */

$this->title = 'Mi cuenta';
$this->params['breadcrumbs'][] = ['label' => 'Cuenta', 'url' => ['cuenta']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pedido-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formCuenta', [
        "model" => $model,
    ]) ?>

</div>