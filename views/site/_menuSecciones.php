<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>
<ul class="menu">
    <li> <?= Html::a('Articulos destacados', Url::to(['site/index'])) ?></li>
    <?php
    foreach ($secciones as $seccion) {
    ?>
        <li>
            <?php
            echo Html::a($seccion->nombre,  ['seccion', 'id' => $seccion->idSeccion]);
            ?>
        </li>
    <?php
    }
    ?>
</ul>

<style>
    ul.menu {
        display: flex;
    }

    ul.menu>li {
        list-style: none;
        margin-right: 30px;
    }

    .activo {
        color: red;
        font-weight: bolder;
    }

    ul.menu>li>a {
        text-decoration: none;
    }
</style>

<script src=" https://code.jquery.com/jquery-3.6.4.min.js" integrity="sha256-oP6HI9z1XaZNBrJURtCoUT5SUnxFr8s3BzRl+cbzUq8=" crossorigin="anonymous"></script>

<script>
    $(document).ready(function() {
        $("ul li a").each(function() {
            if ($(location).attr('href').indexOf($(this).attr("href")) >= 0)
                $(this).addClass("activo");
        });
    });
</script>