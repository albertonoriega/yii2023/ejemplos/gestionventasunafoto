<?php

use yii\helpers\Html;

$this->title = 'Registro completado';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<?php
(!empty($nombre)) ? '<p> Hola' . Html::encode($nombre) . '</p>' : '';
?>

<p>Te has registrado correctamente</p>

<?= Html::a('Iniciar sesión', ['site/login'], ['class' => 'btn btn-primary']) ?>