<?php

use app\models\Usuario;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Usuarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuario-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Usuario', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            'username',
            'password',
            //'isAdmin',
            [
                'attribute' => 'isAdmin',
                'format' => 'raw',
                'value' => function ($dato) {
                    return    $dato->isAdmin  ? '<i class="fas fa-check-square"></i>' : '<i class="far fa-square"></i>';
                }
            ],
            [
                'attribute' => 'cliente',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('Ver cliente', ['cliente/view', 'idCliente' => $model->id]);
                }
            ],

            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Usuario $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],
        ],
    ]); ?>


</div>