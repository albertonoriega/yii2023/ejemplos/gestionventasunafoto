<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Cliente $model */

$this->title = $model->idCliente;
$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="cliente-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'idCliente' => $model->idCliente], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'idCliente' => $model->idCliente], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro que quieres eliminar el registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idCliente',
            'nombre',
            'apellidos',
            'correo',
            'direccion',
            'poblacion',
            'codigoPostal',
            'telefono',
            'telefono2',
            'usuario',
        ],
    ]) ?>

    <h2 style="margin-top: 60px; margin-bottom:40px">Pedidos realizados por <?= $model->getNombrecompleto() ?></h2>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idPedido',
            [
                'attribute' => 'idProducto',
                'value' => function ($model) {
                    return $model->idProducto0->productoconseccion;
                }
            ],
            'cantidad',
            //'fechaPedido',
            [
                'attribute' => 'fechaPedido',
                'value' => function ($model) {
                    return $model->fechaEspana($model->fechaPedido);
                }
            ],
            //'fechaEnvio',
            [
                'attribute' => 'fechaEnvio',
                'value' => function ($model) {
                    return $model->fechaEspana($model->fechaEnvio);
                }
            ],
            //'idCliente',
            //'idProducto',

        ],
    ]); ?>


</div>