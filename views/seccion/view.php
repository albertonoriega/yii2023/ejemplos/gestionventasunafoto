<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Seccion $model */

$this->title = $model->idSeccion;
$this->params['breadcrumbs'][] = ['label' => 'Secciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="seccion-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'idSeccion' => $model->idSeccion], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'idSeccion' => $model->idSeccion], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro que quieres eliminar este registro?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Ver productos', ['site/productos', 'id' => $model->idSeccion], ['class' => 'btn btn-warning']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idSeccion',
            'nombre',
        ],
    ]) ?>

</div>