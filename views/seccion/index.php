<?php

use app\models\Seccion;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Secciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seccion-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="fas fa-plus-circle" style="font-size:1.3em;position:relative;top:2px;"></i> Crear nueva sección', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idSeccion',
            'nombre',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Seccion $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idSeccion' => $model->idSeccion]);
                }
            ],
        ],
    ]); ?>


</div>