<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Seccion $model */

$this->title = 'Actualizar sección: ' . $model->idSeccion;
$this->params['breadcrumbs'][] = ['label' => 'Secciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idSeccion, 'url' => ['view', 'idSeccion' => $model->idSeccion]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="seccion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>