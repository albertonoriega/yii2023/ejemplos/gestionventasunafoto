<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Pedido $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="pedido-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idPedido')->input('number') ?>

    <?= $form->field($model, 'cantidad')->input('number') ?>

    <?= $form->field($model, 'fechaPedido')->input('date') ?>

    <?= $form->field($model, 'fechaEnvio')->input('date') ?>

    <?= $form->field($model, 'idCliente')->dropDownList($model->clientes, ['prompt' => 'Selecciona cliente']) ?>

    <?= $form->field($model, 'idProducto')->dropDownList($model->productos, ['prompt' => 'Selecciona producto']) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>