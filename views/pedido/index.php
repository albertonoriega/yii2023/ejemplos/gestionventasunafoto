<?php

use app\models\Pedido;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Pedidos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pedido-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="fas fa-plus-circle"></i> Nuevo pedido', ['create'], ['class' => 'btn btn-success btn-rounded']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idPedido',
            [
                'attribute' => 'idProducto',
                'value' => function ($model) {
                    return $model->idProducto0->productoconseccion;
                }
            ],
            [
                'attribute' => 'idCliente',
                'value' => function ($model) {
                    return $model->idCliente0->nombrecompleto;
                }
            ],
            'cantidad',
            //'fechaPedido',
            [
                'attribute' => 'fechaPedido',
                'value' => function ($model) {
                    return $model->fechaEspana($model->fechaPedido);
                }
            ],
            //'fechaEnvio',
            [
                'attribute' => 'fechaEnvio',
                'value' => function ($model) {
                    return $model->fechaEspana($model->fechaEnvio);
                }
            ],
            //'idCliente',

            //'idProducto',

            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Pedido $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idPedido' => $model->idPedido]);
                }
            ],
        ],
    ]); ?>


</div>