<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Pedido $model */

$this->title = $model->idPedido;
$this->params['breadcrumbs'][] = ['label' => 'Pedidos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="pedido-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'idPedido' => $model->idPedido], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'idPedido' => $model->idPedido], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro que quieres eliminar el registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idPedido',
            'cantidad',
            'fechaPedido',
            'fechaEnvio',
            //'idCliente',
            [
                'attribute' => 'idCliente',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->idCliente0->nombrecompleto . Html::a('Ver Cliente', ['cliente/view', 'idCliente' => $model->idCliente0->idCliente], ['class' => 'btn btn-warning', 'style' => 'margin-left:20px']);
                }
            ],
            //'idProducto',
            [
                'attribute' => 'idProducto',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->idProducto0->productoconseccion . Html::a('Ver producto', ['producto/view', 'idProducto' => $model->idProducto0->idProducto], ['class' => 'btn btn-warning', 'style' => 'margin-left:20px']);
                }
            ],
        ],
    ]) ?>

</div>