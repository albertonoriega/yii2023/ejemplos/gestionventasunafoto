<?php

use yii\bootstrap5\Html;
use yii\bootstrap5\Nav;


echo Nav::widget([
    'options' => ['class' => 'navbar-nav '],
    'items' => [
        ['label' => 'Ofertas', 'url' => ['/site/ofertas']],
        ['label' => 'Productos', 'url' => ['/site/secciones']],
    ]
]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav ms-auto'],
    'items' => [
        ['label' => ' Mis pedidos', 'url' => ['/site/verpedidos', 'idCliente' => Yii::$app->user->identity->id], 'linkOptions' => ['class' => 'fas fa-shopping-cart', 'style' => 'position:relative;top:4px']],
        ['label' => 'Mi cuenta  ', 'url' => ['/site/cuenta', 'idUsuario' => Yii::$app->user->identity->id]],

        Yii::$app->user->isGuest
            ? ['label' => ' Iniciar sesión', 'url' => ['/site/login'], 'linkOptions' => ['class' => 'far fa-user-circle', 'style' => 'font-size:1.1em;position:relative;top:4px']]
            : '<li class="nav-item">'
            . Html::beginForm(['/site/logout'])
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->nombre . ')',
                ['class' => 'nav-link btn btn-link logout']
            )
            . Html::endForm()
            . '</li>'
    ]
]);
