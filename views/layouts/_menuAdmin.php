<?php

use yii\bootstrap5\Html;
use yii\bootstrap5\Nav;

echo Nav::widget([
    'options' => ['class' => 'navbar-nav'],
    'items' => [
        ['label' => 'Ofertas', 'url' => ['/site/ofertas']],
        ['label' => 'Productos', 'url' => ['/site/secciones']],
        ['label' => 'Administración', 'items' => [
            ['label' => 'Pedidos', 'url' => ['/pedido/index']],
            ['label' => 'Clientes', 'url' => ['/cliente/index']],
            ['label' => 'Productos', 'url' => ['/producto/index']],
            ['label' => 'Secciones', 'url' => ['/seccion/index']],
            ['label' => 'Usuarios', 'url' => ['/usuario/index']],
        ], 'style' => 'background-color:red;'],

        Yii::$app->user->isGuest
            ? ['label' => ' Iniciar sesión', 'url' => ['/site/login'], 'linkOptions' => ['class' => 'far fa-user-circle', 'style' => 'font-size:1.1em;position:relative;top:4px']]
            : '<li class="nav-item">'
            . Html::beginForm(['/site/logout'])
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->nombre . ')',
                ['class' => 'nav-link btn btn-link logout']
            )
            . Html::endForm()
            . '</li>'
    ]
]);
