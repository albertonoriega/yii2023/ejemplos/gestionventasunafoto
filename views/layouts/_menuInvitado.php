<?php

use yii\bootstrap5\Nav;

echo Nav::widget([
    'options' => ['class' => 'navbar-nav'],
    'items' => [
        ['label' => 'Ofertas', 'url' => ['/site/ofertas']],
        ['label' => 'Productos', 'url' => ['/site/secciones']],
    ]
]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav ms-auto'],
    'items' => [
        ['label' => 'Registrarse', 'url' => ['/site/registro']],
        ['label' => ' Login ', 'url' => ['/site/login'], 'linkOptions' => ['class' => "fas fa-user", 'style' => 'position:relative;top:5px']],
    ]
]);
