<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Producto $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="producto-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idProducto')->input('number') ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'precio')->input('float') ?>

    <?= $form->field($model, 'descuento')->input('number') ?>

    <?= $form->field($model, 'oferta')->checkbox() ?>

    <?= $form->field($model, 'destacado')->checkbox() ?>

    <?= $form->field($model, 'unidadesStock')->input('number') ?>

    <?= $form->field($model, 'seccion')->dropDownList($model->secciones, ['prompt' => 'Selecciona sección']) ?>

    <?= $form->field($model, 'archivo')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>