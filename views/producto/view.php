<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Producto $model */

$this->title = $model->idProducto;
$this->params['breadcrumbs'][] = ['label' => 'Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="producto-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'idProducto' => $model->idProducto], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'idProducto' => $model->idProducto], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro que quieres eliminar el registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idProducto',
            'nombre',
            'descripcion',
            //'precio',
            [
                'attribute' => 'precio',
                'format' => 'raw',
                'value' => function ($dato) {
                    if (isset($dato->precio)) {
                        return    $dato->precio  . ' €';
                    }
                }
            ],
            //'descuento',
            [
                'attribute' => 'descuento',
                'format' => 'raw',
                'value' => function ($dato) {
                    if (isset($dato->descuento)) {
                        return    $dato->descuento  . '%';
                    }
                }
            ],
            [
                'attribute' => 'Precio rebajado',
                'format' => 'raw',
                'value' => function ($dato) {
                    if (isset($dato->precio)) {
                        return    $dato->precioRebajado  . ' €';
                    }
                }
            ],
            //'oferta',
            [
                'attribute' => 'oferta',
                'format' => 'raw',
                'value' => function ($dato) {
                    return    $dato->oferta  ? '<i class="fas fa-check-square"></i>' : '<i class="far fa-square"></i>';
                }
            ],
            //'destacado',
            [
                'attribute' => 'destacado',
                'format' => 'raw',
                'value' => function ($dato) {
                    return    $dato->destacado  ? '<i class="fas fa-check-square"></i>' : '<i class="far fa-square"></i>';
                }
            ],
            'unidadesStock',
            //'seccion',
            [
                'attribute' => 'seccion',
                'value' => function ($model) {
                    return $model->seccion0->nombre;
                }
            ],
            //'foto',
            [
                'label' => 'Foto',
                'attribute' => 'foto',
                'format' => 'raw',
                'value' => function ($dato) {
                    if (isset($dato->foto)) {
                        return Html::img("@web/imgs/productos/{$dato->foto}", ["width" => 200, "height" => 200]);
                    } else {
                        return Html::img("@web/imgs/notfound.png", ["width" => 200, "height" => 200]);
                    }
                }
            ],

        ],
    ]) ?>

</div>