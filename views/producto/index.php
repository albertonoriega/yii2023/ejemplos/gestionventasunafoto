<?php

use app\models\Producto;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Productos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="producto-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="fas fa-plus-circle"></i> Nuevo producto', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idProducto',
            'nombre',
            //'descripcion',
            //'precio',
            [
                'attribute' => 'precio',
                'format' => 'raw',
                'value' => function ($dato) {
                    if (isset($dato->precio)) {
                        return    $dato->precio  . ' €';
                    }
                }
            ],
            //'descuento',
            [
                'attribute' => 'descuento',
                'format' => 'raw',
                'value' => function ($dato) {
                    if (isset($dato->descuento)) {
                        return    $dato->descuento  . '%';
                    }
                }
            ],
            [
                'attribute' => 'Precio rebajado',
                'format' => 'raw',
                'value' => function ($dato) {
                    if (isset($dato->precio)) {
                        return    $dato->precioRebajado  . ' €';
                    }
                }
            ],
            // 'oferta',
            [
                'attribute' => 'oferta',
                'format' => 'raw',
                'value' => function ($dato) {
                    return    $dato->oferta  ? '<i class="fas fa-check-square"></i>' : '<i class="far fa-square"></i>';
                }
            ],
            // 'destacado',
            [
                'attribute' => 'destacado',
                'format' => 'raw',
                'value' => function ($dato) {
                    return    $dato->destacado  ? '<i class="fas fa-check-square"></i>' : '<i class="far fa-square"></i>';
                }
            ],
            //'unidadesStock',
            //'seccion',
            [
                'attribute' => 'seccion',
                'value' => function ($model) {
                    return $model->seccion0->nombre;
                }
            ],
            //'foto',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Producto $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idProducto' => $model->idProducto]);
                }
            ],
        ],
    ]); ?>


</div>