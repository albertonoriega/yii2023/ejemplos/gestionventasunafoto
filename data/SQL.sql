﻿DROP DATABASE IF EXISTS gestionVentas2023;

CREATE DATABASE gestionVentas2023;

USE gestionVentas2023;

CREATE TABLE producto(
idProducto int,
nombre varchar (100),
descripcion varchar (800),
precio float,
descuento float,
oferta boolean,
unidadesStock int,
seccion int,
foto varchar(100) NOT NULL,
PRIMARY KEY (idProducto)
);

CREATE TABLE seccion(
idSeccion int,
nombre varchar(100),
PRIMARY KEY (idSeccion)
);

CREATE TABLE cliente( 
idCliente int, 
nombre varchar(100),
apellidos varchar(100),
correo varchar(100),
direccion varchar(200),
poblacion varchar(100),
codigoPostal varchar(20),
telefono int,
telefono2 int,
PRIMARY KEY(idCliente)
);

CREATE TABLE pedido(
idPedido int,
cantidad int,
fechaPedido date,
fechaEnvio date,
idCliente int,
idProducto int,
PRIMARY KEY (idPedido)
);

ALTER TABLE pedido
ADD CONSTRAINT fkPedidoCliente FOREIGN KEY(idCliente) REFERENCES cliente(idCliente),
ADD CONSTRAINT fkPedidoProducto FOREIGN KEY(idProducto) REFERENCES producto(idProducto);

ALTER TABLE producto
ADD CONSTRAINT fkProductoSeccion FOREIGN KEY(seccion) REFERENCES seccion(idSeccion);





