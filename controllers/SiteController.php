<?php

namespace app\controllers;

use app\models\Cliente;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Pedido;
use app\models\Producto;
use app\models\Seccion;
use app\models\Usuario;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        $secciones = Seccion::find()->all();


        return $this->render('index', [
            'secciones' => $secciones
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionSecciones()
    {
        $secciones = Seccion::find()->all();

        return $this->render('secciones', [
            'secciones' => $secciones
        ]);
    }

    public function actionSeccion($id)
    {
        $productos = Producto::find()->where(['seccion' => $id])->andWhere(['destacado' => 1])->all();
        $secciones = Seccion::find()->all();

        return $this->render('seccion', [
            'productos' => $productos,
            'secciones' => $secciones
        ]);
    }


    public function actionProductos($id)
    {
        $datos = Producto::find()->where(['seccion' => $id])->all();

        $dato = Producto::find()->where(['seccion' => $id])->one();

        return $this->render("productos", [
            "datos" => $datos,
            "titulo" => "Productos de la sección: " . $dato->seccion0->nombre,

        ]);
    }

    public function actionVerproducto($idProducto)
    {
        $consulta = Producto::find()->where(['idProducto' => $idProducto]);

        $dato = $consulta->one();

        return $this->render('verproducto', [
            'dato' => $dato,
        ]);
    }

    public function actionOfertas()
    {
        $secciones = Seccion::find()->all();

        return $this->render('ofertasListview', [
            'secciones' => $secciones
        ]);
    }

    // Accion de registro
    public function actionRegistro()
    {
        $model = new Usuario();

        if (Yii::$app->request->isPost) {
            $postData = Yii::$app->request->post();

            $model->load($postData);

            if ($model->validate() && $model->save()) {

                return $this->redirect(['site/success']);
            }
        }

        return $this->render('registro', ['model' => $model]);
    }

    // Accion cuando el usuario se ha registrado correctamente
    public function actionSuccess()
    {
        return $this->render('success');
    }

    // Accion para hacer un pedido
    public function actionHacerpedido($idProducto, $idUsuario = 0)
    {
        // Si el usuario no esta logeado => que se logee
        if (Yii::$app->user->isGuest || $idUsuario == null) {
            return $this->redirect(['site/login']);
        }

        // Si es la primera vez que el usuario hace un pedido, tenemos que crear un registro en la tabla cliente

        $cliente = Cliente::findOne(['usuario' => $idUsuario]);
        if ($cliente  == null) {
            // return $this->redirect(['/site/crearcliente', 'idCliente' => $idUsuario]);
            $user = Usuario::findOne($idUsuario);
            $cliente = new Cliente();
            $cliente->idCliente = $user->id;
            $cliente->nombre = $user->nombre;
            $cliente->correo = $user->username;
            $cliente->usuario = $user->id;
            $cliente->save();
        }

        // Para hacer un pedido se necesita que el cliente tenga direccion y telefono
        if ($cliente->direccion == "" || $cliente->direccion == null || $cliente->telefono == "" || $cliente->telefono == "") {
            Yii::$app->session->setFlash('error', 'Necesitamos tu dirección y tu teléfono');
            return $this->redirect(['cuenta', 'idUsuario' => $cliente->usuario]);
        }

        // Creamos un modelo vacio para pedidos
        $model = new Pedido();
        $producto = Producto::findOne($idProducto);
        $cliente = Cliente::findOne($idUsuario);

        $model->idCliente = $cliente->idCliente;
        $model->idProducto = $producto->idProducto;

        if ($this->request->isPost) {
            // Comprobamos si la cantidad solicitada es menor o igual que las unidades que se tiene en stock
            if ($model->load($this->request->post()) && $model->save() && $model->cantidad <= $producto->unidadesStock) {

                // Si se hace el pedido, las unidades en stock se modifican
                $producto->unidadesStock -= $model->cantidad;
                $producto->save();

                return $this->redirect(['pedido', 'idCliente' => $cliente->idCliente]);
                // Si la cantidad supera las unidades en stock, se muestra un mensaje de error
            } else {
                Yii::$app->session->setFlash('error', 'No disponemos de tantas unidades. Tenemos ' . $producto->unidadesStock . ' unidades');
                $model->delete();
            }
        } else {
            $model->loadDefaultValues();
        }

        // cargamos el formulario para hacer un pedido
        return $this->render('hacerpedido', [
            'model' => $model,
            "cliente" => $cliente,
            "producto" => $producto,
        ]);
    }

    // Accion cuando el usuario ha realizado un pedido correctamente
    public function actionPedido($idCliente)
    {
        return $this->render('pedido', [
            'idCliente' => $idCliente
        ]);
    }

    public function actionVerpedidos($idCliente)
    {

        $dataProvider = new ActiveDataProvider([
            'query' => Pedido::find()->where(['idCliente' => $idCliente]),

            'pagination' => [
                'pageSize' => 10
            ],
            /*
            'sort' => [
                'defaultOrder' => [
                    'idPedido' => SORT_DESC,
                ]
            ],
            */

        ]);

        return $this->render('verpedidos', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionCuenta($idUsuario)
    {
        // Si el usuario no esta logeado => que se logee
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        // Si es la primera vez que el usuario hace un pedido, tenemos que crear un registro en la tabla cliente
        if ($cliente = Cliente::findOne(['idCliente' => $idUsuario]) == null) {
            // return $this->redirect(['/site/crearcliente', 'idCliente' => $idUsuario]);
            $user = Usuario::findOne($idUsuario);
            $cliente = new Cliente();
            $cliente->idCliente = $user->id;
            $cliente->nombre = $user->nombre;
            $cliente->correo = $user->username;
            $cliente->usuario = $user->id;
            $cliente->save();
        }

        $model = Cliente::findOne($idUsuario);

        return $this->render('cuenta', [
            "model" => $model,
        ]);
    }


    // ACTUALIZAR DATOS DEL USUARIO
    public function actionActualizarcuenta($idCliente)
    {
        // Si el usuario no esta logeado => que se logee
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $model = Cliente::findOne(['idCliente' => $idCliente]);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            $usuario = Usuario::findOne(['id' => $model->usuario]);
            // UPDATE
            // Si se cambia el correo o el nombre del cliente, se la cambiamos también a la tabla usuario 
            $connection = Yii::$app->db;
            $connection->createCommand()->update('usuario', ['nombre' => $model->nombre, 'username' => $model->correo], ['id' => $usuario->id])->execute();

            return $this->redirect(['cuenta', 'idUsuario' => $model->usuario]);
        }

        return $this->render('actualizarcuenta', [
            'model' => $model,
        ]);
    }

    // Cancelar un pedido
    public function actionCancelarpedido($idPedido)
    {
        $pedido = Pedido::findOne(['idPedido' => $idPedido]);

        $producto = Producto::findOne(['idProducto' => $pedido->idProducto]);

        // Comparamos la fecha de envio con la fecha actual
        if ($pedido->fechaEnvio >= Yii::$app->formatter->asDate('now', 'yyyy-MM-dd')) {

            // Si se elimina el pedido, se suma la cantidad del pedido al stock y se guarda en la BBDD
            $producto->unidadesStock += $pedido->cantidad;
            $producto->save();

            // Borramos el pedido
            $pedido->delete();
        } else {
            // Mostramos un mensaje de error
            Yii::$app->session->setFlash('error', 'No puedes cancelar un pedido que ya ha sido enviado');
        }
        return $this->redirect(['verpedidos', 'idCliente' => $pedido->idCliente]);
    }
}
