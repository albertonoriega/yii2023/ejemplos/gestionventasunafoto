<?php

namespace app\models;

use DateTime;
use Yii;
use yii\helpers\ArrayHelper;

use function PHPUnit\Framework\isNull;

/**
 * This is the model class for table "pedido".
 *
 * @property int $idPedido
 * @property int|null $cantidad
 * @property string|null $fechaPedido
 * @property string|null $fechaEnvio
 * @property int|null $idCliente
 * @property int|null $idProducto
 *
 * @property Cliente $idCliente0
 * @property Producto $idProducto0
 */
class Pedido extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pedido';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // [['idPedido'], 'required'],
            [['idPedido', 'cantidad', 'idCliente', 'idProducto'], 'integer'],
            [['fechaPedido', 'fechaEnvio'], 'date', 'format' => 'yyyy-MM-dd'],
            [['cantidad'], 'required',],
            [['cantidad'], 'integer', 'min' => 1, 'tooSmall' => 'La {attribute} debe ser mayor de 0'],
            // La fecha de envio será la fecha actual
            [['fechaPedido'], 'default', 'value' => Yii::$app->formatter->asDate('now', 'yyyy-MM-dd')],
            // Le fecha de envio será por defecto, dos días despues de realizar el envio
            [['fechaEnvio'], 'default', 'value' => date('y-m-d', strtotime(Yii::$app->formatter->asDate('now', 'yyyy-MM-dd') . ' + 2 days'))],
            [['idPedido'], 'unique'],
            [['idCliente'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::class, 'targetAttribute' => ['idCliente' => 'idCliente']],
            [['idProducto'], 'exist', 'skipOnError' => true, 'targetClass' => Producto::class, 'targetAttribute' => ['idProducto' => 'idProducto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idPedido' => 'ID pedido',
            'cantidad' => 'Cantidad',
            'fechaPedido' => 'Fecha pedido',
            'fechaEnvio' => 'Fecha de envío',
            'idCliente' => 'Cliente',
            'idProducto' => 'Producto',
        ];
    }

    /**
     * Gets query for [[IdCliente0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdCliente0()
    {
        return $this->hasOne(Cliente::class, ['idCliente' => 'idCliente']);
    }

    /**
     * Gets query for [[IdProducto0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdProducto0()
    {
        return $this->hasOne(Producto::class, ['idProducto' => 'idProducto']);
    }

    public function getProductos()
    {
        $productos = Producto::find()->all();
        return ArrayHelper::map($productos, 'idProducto', 'productoconseccion');
    }

    public function getClientes()
    {
        $clientes = Cliente::find()->all();
        return ArrayHelper::map($clientes, 'idCliente', 'nombrecompleto');
    }

    public function fechaEspana($fecha)
    {
        if (isset($fecha)) {
            return Yii::$app->formatter->asDate($fecha, 'dd-MM-yyyy');
        }
    }

    // Como no metemos id en el formulario, le vamos sumando una en funcion del numero de registros
    public function beforeValidate()
    {
        // Como no metemos id 
        if (isNull($this->idPedido)) {
            // contamos cuantos usuarios hay y le añadimos uno => COMO BORRES REGISTRO DA ERROR PORQUE ALGUN ID VA A COINCIDIR
            // $this->idPedido = Pedido::findBySql('select count(*) from pedido')->scalar() + 1;

            //Como sumar 1 al total de los registros da error:
            // Sacamos el regitro con el id mas alto y le sumamos 1
            $ultimoPedido = Pedido::find()->orderby(['idPedido' => SORT_DESC])->one();
            $ultimoID = $ultimoPedido['idPedido'];
            $this->idPedido = $ultimoID + 1;
        }
        return true;
    }
}
