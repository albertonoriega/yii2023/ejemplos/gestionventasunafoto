<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "producto".
 *
 * @property int $idProducto
 * @property string|null $nombre
 * @property string|null $descripcion
 * @property float|null $precio
 * @property float|null $descuento
 * @property int|null $oferta
 * @property int|null $unidadesStock
 * @property int|null $seccion
 * @property string $foto
 * @property int|null $destacado 
 *
 * @property Pedido[] $pedidos
 * @property Seccion $seccion0
 */
class Producto extends \yii\db\ActiveRecord
{

    public $archivo;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'producto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idProducto'], 'required'],
            [['idProducto', 'oferta', 'unidadesStock', 'seccion', 'destacado'], 'integer'],
            [['precio'], 'number'],
            [['descuento'], 'number', 'min' => 0, 'max' => 100],
            [['nombre', 'foto'], 'string', 'max' => 100],
            [['descripcion'], 'string', 'max' => 800],
            [['archivo'], 'file',  'skipOnEmpty' => true, 'mimeTypes' => 'image/*'],
            [['idProducto'], 'unique'],
            [['seccion'], 'exist', 'skipOnError' => true, 'targetClass' => Seccion::class, 'targetAttribute' => ['seccion' => 'idSeccion']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idProducto' => 'ID producto',
            'nombre' => 'Nombre del producto',
            'descripcion' => 'Descripción',
            'precio' => 'Precio',
            'descuento' => 'Descuento',
            'oferta' => 'Oferta',
            'unidadesStock' => 'Unidades en stock',
            'seccion' => 'Sección',
            'foto' => 'Foto',
            'destacado' => 'Destacado',
        ];
    }

    /**
     * Gets query for [[Pedidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPedidos()
    {
        return $this->hasMany(Pedido::class, ['idProducto' => 'idProducto']);
    }

    /**
     * Gets query for [[Seccion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSeccion0()
    {
        return $this->hasOne(Seccion::class, ['idSeccion' => 'seccion']);
    }

    public function getSecciones()
    {
        $secciones = Seccion::find()->all();
        return ArrayHelper::map($secciones, 'idSeccion', 'nombre');
    }

    public function getProductoconseccion()
    {
        return $this->nombre . " ({$this->seccion0->nombre})";
    }


    public function beforeValidate(): bool
    {
        // Comprobamos si has seleccionado una foto 
        if (isset($this->archivo)) {
            $this->archivo = UploadedFile::getInstance($this, "archivo");
        }
        return true; // este metodo te pide que devuelvas true. Si no devuelves true no hace nada
    }

    public function afterValidate(): bool
    {
        // Volvemos a comprobar si se ha subido una foto
        if (isset($this->archivo)) {
            $this->subirArchivo();
            $this->foto = $this->idProducto . '_' . $this->archivo->name; // Al campo foto le guardamos el nombre de la foto
        }
        return true; // este metodo te pide que devuelvas true. Si no devuelves true no hace nada
    }


    public function subirArchivo(): bool
    {
        $this->archivo->saveAs('imgs/productos/' . $this->idProducto . '_' . $this->archivo->name, false);  // false, si la foto existe, no sube la nueva para no destruir la que está
        return true;
    }

    // Disparador que se ejecuta después de eliminar un registro
    public function afterDelete()
    {
        // Elimino la imagen de web/imgs/productos
        if ($this->foto != null && file_exists(Yii::getAlias("@webroot") . '/imgs/productos/' . $this->foto)) {
            unlink('imgs/productos/' . $this->foto);
        }
        return true;
    }
    /**
     * afterSave 
     * Este método se ejecuta después de guardar el registro en la BBDD
     * 
     * @param  mixed $insert este argumento es true si se está insertando un registro y false si es una actualización
     * @param  array $atributosAnteriores Array con todos los datos de la tabla antes de actualizar
     * @return void
     */
    public function afterSave($insert, $atributosAnteriores)
    {
        // Si estoy actualizando datos
        if (!$insert) {
            // Si el prodcuto tenía ya una foto y hemos adjuntado una nueva, elimina la vieja
            if (isset($this->archivo) && isset($atributosAnteriores['foto'])) {
                unlink('imgs/productos/' . $atributosAnteriores['foto']);
            }
        }
    }

    public function getPrecioeuros()
    {
        return $this->precio . " €";
    }

    public function getPrecioRebajado()
    {
        if ($this->oferta && isset($this->descuento)) {
            return round($this->precio - ($this->precio * $this->descuento / 100), 2);
        } else {
            return $this->precio;
        }
    }
}
