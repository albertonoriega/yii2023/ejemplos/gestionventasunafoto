<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

use function PHPUnit\Framework\isNull;

/**
 * This is the model class for table "cliente".
 *
 * @property int $idCliente
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $correo
 * @property string|null $direccion
 * @property string|null $poblacion
 * @property string|null $codigoPostal
 * @property int|null $telefono
 * @property int|null $telefono2
 * @property int|null $usuario 
 *
 * @property Pedido[] $pedidos
 */
class Cliente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cliente';
    }

    public function rules()
    {
        return [

            [['idCliente', 'telefono', 'telefono2', 'usuario'], 'integer'],
            [['nombre', 'apellidos', 'poblacion'], 'string', 'max' => 100],
            [['telefono', 'telefono2'], 'integer', 'min' => 0, 'max' => 999999999, 'tooBig' => 'Introduce un número de máximo 9 caracteres'],
            [['correo'], 'string'],
            [['direccion'], 'string', 'max' => 200],
            [['codigoPostal'], 'string', 'max' => 20],
            [['idCliente'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idCliente' => 'ID cliente',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'correo' => 'Correo electrónico',
            'direccion' => 'Dirección',
            'poblacion' => 'Población',
            'codigoPostal' => 'Código postal',
            'telefono' => 'Telefono',
            'telefono2' => 'Telefono 2',
            'usuario' => 'usuario'
        ];
    }

    /**
     * Gets query for [[Pedidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPedidos()
    {
        return $this->hasMany(Pedido::class, ['idCliente' => 'idCliente']);
    }

    public function getNombrecompleto()
    {
        return $this->nombre . " " . $this->apellidos;
    }

    /**
     * Gets query for [[Usuario0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario0()
    {
        return $this->hasOne(Usuario::class, ['id' => 'usuario']);
    }

    public function getUsuarios()
    {
        $usuarios = Usuario::find()->all();
        return ArrayHelper::map($usuarios, 'id', 'username');
    }
}
