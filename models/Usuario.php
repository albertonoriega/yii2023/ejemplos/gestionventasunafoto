<?php

namespace app\models;

use Yii;
use yii\base\Security;

use function PHPUnit\Framework\isNull;

/**
 * This is the model class for table "usuario".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $username
 * @property string|null $password
 * @property int|0 $isAdmin
 *
 * @property Cliente $cliente
 */
class Usuario extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // Por defecto isAdmin sera 0, al registrase, los usuarios no seran administrador
            ['isAdmin', 'default', 'value' => 0],
            [['nombre', 'username', 'password'], 'string', 'max' => 100],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'username' => 'Username',
            'password' => 'Password',
            'isAdmin' => 'Is Admin',
        ];
    }

    /**
     * Gets query for [[Cliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCliente()
    {
        return $this->hasOne(Cliente::class, ['usuario' => 'id']);
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /* public function validatePassword($password)
      {
      return Yii::$app->security->validatePassword($password, $this->password_hash);
      } */

    public function validatePassword($password)
    {
        if (is_null($this->password))
            return false;
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return true;
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    public function beforeValidate()
    {
        // Como no metemos id contamos cuantos usuarios hay y le añadimos uno
        if (isNull($this->id)) {
            $this->id = Usuario::findBySql('select count(*) from usuario')->scalar() + 1;

            //Como sumar 1 al total de los registros da error:
            // Sacamos el regitro con el id mas alto y le sumamos 1
            $ultimoUsuario = Usuario::find()->orderby(['id' => SORT_DESC])->one();
            $ultimoID = $ultimoUsuario['id'];
            $this->id = $ultimoID + 1;
        }
        return true;
    }

    public function beforeSave($insert)
    {

        if (parent::beforeSave($insert)) {

            if ($this->isNewRecord) {
                $this->password = Yii::$app->security->generatePasswordHash($this->password);
            } else {
                if (!empty($this->getDirtyAttributes(["password"]))) {
                    $this->password = Yii::$app->security->generatePasswordHash($this->password);
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Comprobamos si el usuario es administrador
     * @param mixed $id
     * @return bool
     */
    public static function isUserAdmin($id)
    {
        if (Usuario::findOne(['id' => $id, 'administrador' => true])) {
            return true;
        } else {

            return false;
        }
    }

    /**
     * Summary of isUserSimple
     * @param mixed $id
     * @return bool
     */
    public static function isUserSimple($id)
    {
        if (Usuario::findOne(['id' => $id, 'administrador' => false])) {
            return true;
        } else {

            return false;
        }
    }
}
