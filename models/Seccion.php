<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "seccion".
 *
 * @property int $idSeccion
 * @property string|null $nombre
 *
 * @property Producto[] $productos
 */
class Seccion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'seccion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idSeccion'], 'required'],
            [['idSeccion'], 'integer'],
            [['nombre'], 'string', 'max' => 100],
            [['idSeccion'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idSeccion' => 'ID seccion',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[Productos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductos()
    {
        return $this->hasMany(Producto::class, ['seccion' => 'idSeccion']);
    }

    public function getProductosOferta()
    {
        return $this->hasMany(Producto::class, ['seccion' => 'idSeccion'])->where(['oferta' => 1]);
    }

    public function getProductosDestacado()
    {
        return $this->hasMany(Producto::class, ['seccion' => 'idSeccion'])->where(['destacado' => 1]);
    }
}
